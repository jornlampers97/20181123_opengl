//
// Created by jornl on 3-12-2018.
//

#include "Entity.h"

Entity::Entity(GLuint shaderID, Mesh *mesh, Material *mat) : Model(shaderID, mesh, mat) {
    this->mass = 1.0;
    this->velocity = vec3(0,0,0);
}

Entity::Entity(GLuint shaderID, Mesh *mesh, Material *mat, double mass) : Model(shaderID, mesh, mat) {
    this->mass = mass;
    this->velocity = vec3(0,0,0);
}

void Entity::tick(long micros) {
    double secs = (double) micros / 1000000;

    rotate(0, secs * 0.000001, 0);

    vec3 pos = vec3(posX, posY, posZ);
    pos = pos.add(velocity.multiply(secs));

    posX = pos.x; posY = pos.y; posZ = pos.z;
}

vec3 Entity::getVelocity() {
    return velocity;
}

double Entity::getMass() {
    return mass;
}

void Entity::setMass(double mass) {
    this->mass = mass;
}

void Entity::addMass(double mass) {
    this->mass += mass;
}

void Entity::addVelocity(vec3 v) {
    this->velocity = this->velocity.add(v);
}

void Entity::setVelocity(vec3 v) {
    this->velocity = v;
}

void Entity::applyForce(vec3 f) {
    vec3 ds = f.multiply(1.0/(mass*10));    // 1N -> +1 m/s for an object of 0.1 kg
    this->velocity = velocity + ds;         // Add projected difference in velocity to original velocity
}
