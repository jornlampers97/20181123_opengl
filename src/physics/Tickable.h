//
// Created by jornl on 3-12-2018.
//

#ifndef SHADERS_TICKABLE_H
#define SHADERS_TICKABLE_H


class Tickable {
public:
    virtual void tick(long micros);
};


#endif //SHADERS_TICKABLE_H
