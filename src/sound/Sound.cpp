//
// Created by jorn on 16-12-18.
//

#include "Sound.h"

Sound::Sound(vec3 origin, float loudness, int triggerTick, int ticksToLive, vec3 vel) {
    this->origin = origin;
    this->loudness = loudness;
    this->triggerTick = triggerTick;
    this->ticksToLive = ticksToLive;
    this->originVelocity = vel;
}

float Sound::getLoudness() {
    return loudness;
}

int Sound::getTriggerTick() {
    return triggerTick;
}

int Sound::getTicksToLive() {
    return ticksToLive;
}

vec3 Sound::getVelocity() {
    return originVelocity;
}

bool Sound::hasExpired(int tickNo) {
    return tickNo > triggerTick + ticksToLive;
}

vec3 Sound::getOrigin() {
    return origin;
}
