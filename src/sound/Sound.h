//
// Created by jorn on 16-12-18.
//

#ifndef SHADERS_SOUND_H
#define SHADERS_SOUND_H


#include "../util/vec3.h"

class Sound {
private:
    float loudness;
    int triggerTick;
    int ticksToLive;
    vec3 origin;
    vec3 originVelocity;
protected:
public:
    Sound(vec3 origin, float loudness, int triggerTick, int ticksToLive, vec3 vel);

    float getLoudness();
    int getTriggerTick();
    int getTicksToLive();
    vec3 getVelocity();
    vec3 getOrigin();

    bool hasExpired(int tickNo);
};


#endif //SHADERS_SOUND_H
