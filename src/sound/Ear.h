//
// Created by jorn on 16-12-18.
//

#ifndef SHADERS_EAR_H
#define SHADERS_EAR_H


#include "../util/vec3.h"
#include "Sound.h"

class Ear {
public:
    virtual void hear(Sound * s);
};


#endif //SHADERS_EAR_H
