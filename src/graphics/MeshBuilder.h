//
// Created by jorn on 24-11-18.
//

#ifndef SHADERS_MESHBUILDER_H
#define SHADERS_MESHBUILDER_H

#include "Mesh.h"
#include <iostream>
#include <math.h>
#include "../util/vec3.h"
#include "DisectedMesh.h"

class MeshBuilder {
public:
    static Mesh * constructCube();
    static Mesh * constructSphere(int lats, int longs);
    static Mesh * constructCylinder(int sides);
    static Mesh * constructSymetricalPolygon(int sides);
    static Mesh * constructBox();
    static Mesh * constructQuad();
    static Mesh * constructCone(int divisions);
    static Mesh * constructOctaHedron();
    static Mesh * constructDecaHedron();

    static void invert(Mesh * m);

    static Mesh * constructTerrainRaster(int x, int z, int dx, int dz);

private:
    static void quad(Mesh * m, float bx, float by, float lx, float ly);
    static void quad(Mesh * m, float lx, float ly);
    static void rotate(Mesh * m, float rx, float ry, float rz);
    static void transform(Mesh * m, float dx, float dy, float dz);

};


#endif //SHADERS_MESHBUILDER_H
