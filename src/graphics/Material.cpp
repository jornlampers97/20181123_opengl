//
// Created by jorn on 23-11-18.
//

#include "Material.h"

Material::Material(GLuint shaderID) {
    this->shaderID = shaderID;

    setupGraphics();
}

void Material::setHardness(GLfloat intensity) {
    this->hardness = intensity;
}

void Material::setAmbientColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    color_ambient[0] = r;
    color_ambient[1] = g;
    color_ambient[2] = b;
    color_ambient[3] = a;
}

void Material::setDiffuseColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    color_diffuse[0] = r;
    color_diffuse[1] = g;
    color_diffuse[2] = b;
    color_diffuse[3] = a;
}

void Material::setSpecularColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    color_specular[0] = r;
    color_specular[1] = g;
    color_specular[2] = b;
    color_specular[3] = a;
}

void Material::render() {
    glUniform4fv(ambientID, 1, color_ambient);
    glUniform4fv(diffuseID, 1, color_diffuse);
    glUniform4fv(specularID, 1, color_specular);

    glUniform1f(ambientIntensityID, ambient_intensity);
    glUniform1f(diffuseIntensityID, diffuse_intensity);
    glUniform1f(specularIntensityID, specular_intensity);

    glUniform1f(hardnessID, hardness);

    glUniform1i(ambientMapID, 0);
    glUniform1i(diffuseMapID, 1);
    glUniform1i(specularMapID, 2);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ambient);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_diffuse);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_specular);
}

void Material::setupGraphics() {
    ambientID = glGetUniformLocation(shaderID, "vertex_material_color_ambient");
    diffuseID = glGetUniformLocation(shaderID, "vertex_material_color_diffuse");
    specularID = glGetUniformLocation(shaderID, "vertex_material_color_specular");

    ambientMapID = glGetUniformLocation(shaderID, "texture_ambient");
    diffuseMapID = glGetUniformLocation(shaderID, "texture_diffuse");
    specularMapID = glGetUniformLocation(shaderID, "texture_specular");

    hardnessID = glGetUniformLocation(shaderID, "hardness");

    ambientIntensityID = glGetUniformLocation(shaderID, "vertex_material_ambient_intensity");
    diffuseIntensityID = glGetUniformLocation(shaderID, "vertex_material_diffuse_intensity");
    specularIntensityID = glGetUniformLocation(shaderID, "vertex_material_specular_intensity");

}

void Material::setAmbientTexture(GLuint texID) {
    this->texture_ambient = texID;
}

void Material::setDiffuseTexture(GLuint texID) {
    this->texture_diffuse = texID;
}

void Material::setSpecularTexture(GLuint texID) {
    this->texture_specular = texID;
}

void Material::setAmbientIntensity(float intensity) {
    ambient_intensity = intensity;
}

void Material::setDiffuseIntensity(float intensity) {
    diffuse_intensity = intensity;
}

void Material::setSpecularIntensity(float intensity) {
    specular_intensity = intensity;
}