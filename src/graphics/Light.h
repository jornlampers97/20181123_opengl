//
// Created by jorn on 18-11-18.
//

#ifndef SHADERS_LIGHT_H
#define SHADERS_LIGHT_H

#include <GL/glew.h>
#include "../Camera.h"

class Light : public Preprocessor {

public:
    Light(double locX, double locY, double locZ, GLuint shaderID);
    double * getLocation();
    void setColor(double r, double g, double b, double a);
    void preprocess();
    void displace(GLfloat dx, GLfloat dy, GLfloat dz);
    void setIntensity(GLfloat intensity);

private:
    void setupGraphics();

    double locX, locY, locZ;

    GLfloat loc[4] = {
            0.0f, 0.0f, 0.0f, 1.0f
    };

    GLfloat col[4] = {
            1.0f, 1.0f, 1.0f, 1.0f // Replace this with some kind of array containing BBR frequencies?
    };

    GLfloat intensity;

    GLuint lightPosID;
    GLuint lightColID;
    GLuint lightIntID;

};


#endif //SHADERS_LIGHT_H
