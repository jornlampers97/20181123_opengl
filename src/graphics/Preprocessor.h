//
// Created by jorn on 12-12-18.
//

#ifndef SHADERS_PREPROCESSOR_H
#define SHADERS_PREPROCESSOR_H


#include <GL/glew.h>

class Preprocessor {
public:
    Preprocessor(GLuint shaderID);
    virtual void preprocess();
protected:
    virtual void setupGraphics();
    GLuint shaderID;
    GLuint vao;
};


#endif //SHADERS_PREPROCESSOR_H
