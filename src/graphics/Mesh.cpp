//
// Created by jorn on 24-11-18.
//

#include "Mesh.h"

Mesh::Mesh() {
    vertices = std::vector<GLfloat>();
    normals = std::vector<GLfloat>();
    uvs = std::vector<GLfloat>();
    indices = std::vector<GLuint>();
}

void Mesh::integrate(Mesh * m) {
    int baseIndice = vertices.size() / 3;

    // Copy vertices
    for(int v = 0; v < m->vertices.size(); v++)
        this->vertices.push_back(m->vertices[v]);

    // Copy normals
    for(int n = 0; n < m->normals.size(); n++)
        this->normals.push_back(m->normals[n]);

    // Copy uvs
    for(int u = 0; u < m->uvs.size(); u++)
        this->uvs.push_back(m->uvs[u]);

    // Copy Indices
    for(int i = 0; i < m->indices.size(); i++)
        this->indices.push_back(m->indices[i] + baseIndice);
}

