//
// Created by jorn on 24-11-18.
//

#include "MeshBuilder.h"
#include "../util/SimplexNoise.h"
#include "../util/Heightmap.h"

Mesh * MeshBuilder::constructCube() {

    Mesh * mesh = new Mesh();

    GLfloat vertices[12*6] = { // 6 squares (12 triangles) sharing 8 vertices consisting of 3 floats
            // Back
            -0.5f, -0.5f, -0.5f,
            -0.5f, +0.5f, -0.5f,
            +0.5f, -0.5f, -0.5f,
            +0.5f, +0.5f, -0.5f,
            // Front
            -0.5f, -0.5f, +0.5f,
            -0.5f, +0.5f, +0.5f,
            +0.5f, -0.5f, +0.5f,
            +0.5f, +0.5f, +0.5f,
            // Left
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, +0.5f,
            -0.5f, +0.5f, -0.5f,
            -0.5f, +0.5f, +0.5f,
            // Right
            +0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, -0.5f,
            +0.5f, -0.5f, +0.5f,
            +0.5f, -0.5f, -0.5f,
            // Bottom
            -0.5f, -0.5f, -0.5f,
            +0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, +0.5f,
            +0.5f, -0.5f, +0.5f,
            // Top
            +0.5f, +0.5f, +0.5f,
            -0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, -0.5f,
            -0.5f, +0.5f, -0.5f
    };

    for(int v = 0; v < 12*6; v++)
        mesh->vertices.push_back(vertices[v]);

    GLfloat normals[12*6] = { // 6 squares (12 triangles) sharing 8 normals consisting of 3 floats
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,

            0.0f, 0.0f, +1.0f,
            0.0f, 0.0f, +1.0f,
            0.0f, 0.0f, +1.0f,
            0.0f, 0.0f, +1.0f,

            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,

            +1.0f, 0.0f, 0.0f,
            +1.0f, 0.0f, 0.0f,
            +1.0f, 0.0f, 0.0f,
            +1.0f, 0.0f, 0.0f,

            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,

            0.0f, +1.0f, 0.0f,
            0.0f, +1.0f, 0.0f,
            0.0f, +1.0f, 0.0f,
            0.0f, +1.0f, 0.0f
    };
    for(int n = 0; n < 12*6; n++)
        mesh->normals.push_back(normals[n]);


    GLfloat uvs[12*4] = { // 6 squares (12 triangles) sharing 8 vertices consisting of 3 floats
            // Back
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            // Front
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            // Left
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            // Right
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            // Bottom
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            // Top
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
    };
    for(int v = 0; v < 12*4; v++)
        mesh->uvs.push_back(uvs[v]);

    GLuint indices[6*6] = { // 3 vertices per triangle, 2 triangles per square, 6 squares per cube
            0+0, 1+0, 2+0,
            1+0, 3+0, 2+0,

            2+4, 1+4, 0+4,
            2+4, 3+4, 1+4,

            0+8, 1+8, 2+8,
            1+8, 3+8, 2+8,

            2+12, 1+12, 0+12,
            2+12, 3+12, 1+12,

            0+16, 1+16, 2+16,
            1+16, 3+16, 2+16,

            2+20, 1+20, 0+20,
            2+20, 3+20, 1+20
    };
    for(int i = 0; i < 6*6; i++)
        mesh->indices.push_back(indices[i]);

    return mesh;
}

Mesh * MeshBuilder::constructBox() {
    Mesh * box = new Mesh();

    // Generate one side, transform it 0.5 from the origin, add verts to box, rotate, add verts, rotate etc....
    Mesh * side = new Mesh();
    quad(side, 1.0, 1.0);           // Create a quad, centered at the origin
    transform(side, 0, 0, -0.5);    // Move it half its width or height away from the origin

    // Face north/east/south/west
    box->integrate(side);
    rotate(side, 0, 0.5*M_PI, 0);   // Rotations now effectively make the quad orbit the origin
    box->integrate(side);
    rotate(side, 0, 0.5*M_PI, 0);
    box->integrate(side);
    rotate(side, 0, 0.5*M_PI, 0);
    box->integrate(side);

    rotate(side, 0, 0, 0.5*M_PI);
    box->integrate(side);
    rotate(side, 0, 0, M_PI);
    box->integrate(side);


    delete side;
    return box;
}

Mesh * MeshBuilder::constructQuad() {
    Mesh * q = new Mesh();

    quad(q, -0.5, -0.5, 1.0, 1.0);

    return q;
}

Mesh * MeshBuilder::constructSphere(int lats, int longs) {

    DisectedMesh dm = DisectedMesh();

    float sx = 2 * M_PI / lats;
    float sy = 2 * M_PI / longs;

    int y = 0;

    for(y = 0; y < longs/2; y++) {
        for (int x = 0; x < lats; x++) {
            //vec3 v1 = vec3(cos(sx * x), 0, sin(sx * x));
            vec3 v1 = vec3(cos(sx * x) * cos(sy * (y)), sin(sy * (y)), sin(sx * x) * cos(sy * (y))).multiply(0.5);
            dm.vertices.push_back(v1);
            dm.normals.push_back(v1);

            int i1 = y * lats + (x % lats);
            int i2 = y * lats + ((x + 1) % lats);
            int i3 = y * lats + ((x % lats) + lats);
            int i4 = y * lats + (((x + 1) % lats) + lats);

            dm.indices.push_back(ind3(i3, i2, i1));
            dm.indices.push_back(ind3(i4, i2, i3));
        }

        for (int x = 0; x < lats; x++) {
            vec3 v1 = vec3(cos(sx * x) * cos(sy * (y + 1)), sin(sy * (y + 1)), sin(sx * x) * cos(sy * (y + 1))).multiply(0.5);
            dm.vertices.push_back(v1);
            dm.normals.push_back(v1);
        }
    }

    Mesh * m = dm.construct();
    Mesh * m2 = dm.construct();
    rotate(m2, M_PI, 0, 0);
    m->integrate(m2);

    delete m2;
    return m;
}

// Cylinder, its smooth side has interpolated uvs, making this mesh insuitable for texturing
Mesh * MeshBuilder::constructCylinder(int sides) {
    Mesh * cylinder = new Mesh();

    // Generate and copy 2 symetrical poligons, connect their vertices with indices...
    Mesh * pol = constructSymetricalPolygon(sides);

    // Bottom
    rotate(pol, 0.5 * M_PI, 0, 0);
    transform(pol, 0, 0.5, 0);
    cylinder->integrate(pol);

    // Top
    transform(pol, 0, -1, 0);
    invert(cylinder);
    cylinder->integrate(pol);

    delete pol;

    for(int i = 1; i < sides; i++) {
        int d1 = i;
        int d2 = sides+i+1;
        cylinder->indices.push_back(d2); cylinder->indices.push_back(d1+1); cylinder->indices.push_back(d1);
        cylinder->indices.push_back(d2+1); cylinder->indices.push_back(d1+1); cylinder->indices.push_back(d2);
    }

    cylinder->indices.push_back(sides*2+1); cylinder->indices.push_back(1); cylinder->indices.push_back(sides);
    cylinder->indices.push_back(sides+2); cylinder->indices.push_back(1); cylinder->indices.push_back(sides*2+1);

    return cylinder;
}

Mesh * MeshBuilder::constructSymetricalPolygon(int sides) {
    Mesh * m = new Mesh();
    // The center of the polygon; a point in each triangle of the polygon:
    m->vertices.push_back(0);   m->vertices.push_back(0);   m->vertices.push_back(0); // 0, 0, 0
    m->normals.push_back(0);    m->normals.push_back(0);    m->normals.push_back(1.0);
    m->uvs.push_back(0.5);      m->uvs.push_back(0.5);
    m->indices.push_back(0);    m->indices.push_back(1);    m->indices.push_back(sides);

    for(int i = 0; i < sides; i++) {
        float px = sin(((float)i/sides) * 2 * M_PI); float py = cos(((float)i/sides) * 2 * M_PI);
        m->vertices.push_back(px); m->vertices.push_back(py); m->vertices.push_back(0);
        m->normals.push_back(0);   m->normals.push_back(0);   m->normals.push_back(1.0);
        m->uvs.push_back((px+1.0)/2);      m->uvs.push_back((py+1.0)/2);
        m->indices.push_back(0);   m->indices.push_back(i+1); m->indices.push_back(i);
    }

    return m;
}

Mesh * MeshBuilder::constructTerrainRaster(int rx, int rz, int dx, int dz) {

    DisectedMesh raster = DisectedMesh();

    float stepX = 1.0 / rx;
    float stepZ = 1.0 / rz;

    int startIndice = 0;

    float heights[rx+1][rz+1];

    Heightmap heightmap = Heightmap(11);

    for (int x = 0; x <= rx; x++)
        for (int z = 0; z <= rz; z++) {
            heights[x][z] = heightmap.getHeight(x+dx, z+dz)*5;
        }


    for (int x = 0; x < rx; x++) {
        for (int z = 0; z < rz; z++) {

            vec3 v1 = vec3(-0.5 + stepX * (x+0), heights[x][z],   -0.5 + stepZ * (z+0));
            vec3 v2 = vec3(-0.5 + stepX * (x+0), heights[x][z+1], -0.5 + stepZ * (z+1));
            vec3 v3 = vec3(-0.5 + stepX * (x+1), heights[x+1][z],   -0.5 + stepZ * (z+0));
            vec3 v4 = vec3(-0.5 + stepX * (x+1), heights[x+1][z+1], -0.5 + stepZ * (z+1));

            vec3 na = (v2-v1).cross(v3-v1);
            vec3 nb = (v3-v4).cross(v2-v4);

            raster.vertices.push_back(v1);
            raster.vertices.push_back(v2);
            raster.vertices.push_back(v3);

            raster.vertices.push_back(v4);
            raster.vertices.push_back(v3);
            raster.vertices.push_back(v2);

            //normals
            raster.normals.push_back(na);
            raster.normals.push_back(na);
            raster.normals.push_back(na);

            raster.normals.push_back(nb);
            raster.normals.push_back(nb);
            raster.normals.push_back(nb);

            raster.uvs.push_back(vec2(0,0));
            raster.uvs.push_back(vec2(0,1));
            raster.uvs.push_back(vec2(1,0));

            raster.uvs.push_back(vec2(1,1));
            raster.uvs.push_back(vec2(1,0));
            raster.uvs.push_back(vec2(0,1));

            startIndice = (x * rz  +  z) * 6;

            raster.indices.push_back(ind3(startIndice + 0, startIndice + 1, startIndice + 2));
            raster.indices.push_back(ind3(startIndice + 3, startIndice + 4, startIndice + 5));

        }
    }

    return raster.construct();
}

void MeshBuilder::quad(Mesh * m, float bx, float by, float lx, float ly) {

    float p[4][3];

    p[0][0] = bx;       p[0][1] = by;       p[0][2] = 0;
    p[1][0] = bx;       p[1][1] = by + ly;  p[1][2] = 0;
    p[2][0] = bx + lx;  p[2][1] = by;       p[2][2] = 0;
    p[3][0] = bx + lx;  p[3][1] = by + ly;  p[3][2] = 0;

    float n0123[3] = {0, 0, 1};
    if(m->vertices.size() % 3 != 0) std::cout << "Mesh vertices appear to be corrupt..." << std::endl;
    int baseIndice = m->vertices.size() / 3;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++) {
            m->vertices.push_back(p[i][j]);
            m->normals.push_back(n0123[j]);
        }
    }

    m->indices.push_back(baseIndice + 2);
    m->indices.push_back(baseIndice + 1);
    m->indices.push_back(baseIndice + 0);

    m->indices.push_back(baseIndice + 1);
    m->indices.push_back(baseIndice + 2);
    m->indices.push_back(baseIndice + 3);
}

void MeshBuilder::quad(Mesh * m, float lx, float ly) {

    float bx = 0 - 0.5 * lx;
    float by = 0 - 0.5 * ly;

    quad(m, bx, by, lx, ly);
}

void MeshBuilder::invert(Mesh * m) {
    std::vector<GLuint> newIndices = std::vector<GLuint>();
    for(int i = 0; i < m->indices.size(); i+=3) {
        newIndices.push_back(m->indices[i+2]);
        newIndices.push_back(m->indices[i+1]);
        newIndices.push_back(m->indices[i+0]);
    }
    for(int n = 0; n < m->normals.size(); n++)
        m->normals[n] = -m->normals[n];
    m->indices = newIndices;
}

void MeshBuilder::transform(Mesh * m, float dx, float dy, float dz) {
    vec3 t = vec3(dx, dy, dz);

    for(int i = 0; i < m->vertices.size() / 3; i++) {
        vec3 n = vec3(m->vertices[i*3], m->vertices[i*3+1], m->vertices[i*3+2]).add(t);
        m->vertices[i*3] = n.x;
        m->vertices[i*3+1] = n.y;
        m->vertices[i*3+2] = n.z;
    }
}

void MeshBuilder::rotate(Mesh *m, float rx, float ry, float rz) {
    for(int i = 0; i < m->vertices.size() / 3; i++) {
        vec3 nv = vec3(m->vertices[i*3], m->vertices[i*3+1], m->vertices[i*3+2]).rotateZ(rz).rotateY(ry).rotateX(rx);
        vec3 nn = vec3(m->normals[i*3], m->normals[i*3+1], m->normals[i*3+2]).rotateZ(rz).rotateY(ry).rotateX(rx);

        m->vertices[i*3] = nv.x;
        m->vertices[i*3+1] = nv.y;
        m->vertices[i*3+2] = nv.z;

        m->normals[i*3] = nn.x;
        m->normals[i*3+1] = nn.y;
        m->normals[i*3+2] = nn.z;
    }
}
