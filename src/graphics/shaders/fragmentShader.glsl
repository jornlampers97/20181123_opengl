#version 440

// Vars in world space
uniform float camX;
uniform float camY;
uniform float camZ;

uniform float hardness;

uniform vec4 vertex_material_color_ambient;
uniform vec4 vertex_material_color_diffuse;
uniform vec4 vertex_material_color_specular;

uniform float vertex_material_ambient_intensity;
uniform float vertex_material_diffuse_intensity;
uniform float vertex_material_specular_intensity;

uniform vec4 light_pos;
uniform vec4 light_col;
uniform float light_intensity;

uniform float tick_no;

uniform float light_sensitivity;
uniform float light_visibility_threshold;

uniform sampler2D texture_ambient;
uniform sampler2D texture_diffuse;
uniform sampler2D texture_specular;

in vec3 WVp;   // Worldspace Vertex pos (Interpolated!)
in vec3 WVnN;  // Worldspace vertex normal (Normalized and rotated) (Interpolated!)

in vec2 texCoord;

layout(location = 0) out vec4 fColor;

float ambientIntensity() {
    return vertex_material_ambient_intensity;
}

float diffuseIntensity() {
    vec3 WVLd = light_pos.xyz - WVp;
    vec3 WVLdN = normalize(WVLd); // The direction from vertex to light (inverse lightray dir)

    float distance = length(WVLd.xyz); // The further the light from the vertex, the less the intensity should be (when using point lights)

    // Diffuse intensity increases as inverse light direction to vertex matches interpolated vertex normal
    return max(dot(WVLdN, WVnN.xyz),0) * light_intensity * vertex_material_diffuse_intensity;
}

float specularIntensity() {
    vec3 WVLdN = normalize(light_pos.xyz - WVp); // The direction from vertex to light (inverse lightray dir)
    vec3 WLVref = reflect(-WVLdN, WVnN);          // The direction light from given direction would reflect off of a perfect surface

    vec3 WCp = vec3(camX, camY, camZ);
    vec3 WVCdN = normalize(WCp - WVp); // The direction from given fragment (interpolated vertex) to the camera

    // Specular intensity increases the more direction from fragment to camera matches ...
    // the direction light would be reflected off a perfect surface from given light ray.
    return max(pow(dot(WLVref, WVCdN), hardness), 0) * light_intensity * vertex_material_specular_intensity;
}

void main () {
	vec4 col_amb = texture2D (texture_ambient, texCoord) + vertex_material_color_ambient;
    vec4 col_dif = texture2D (texture_diffuse, texCoord) + vertex_material_color_diffuse;
	vec4 col_spec = texture2D (texture_specular, texCoord) + vertex_material_color_specular;


    vec4 ambient_final = ambientIntensity() * col_amb;
    vec4 diffuse_final = diffuseIntensity() * col_dif * light_col;
    vec4 specular_final = specularIntensity() * col_spec * light_col;

    vec4 threshold = light_visibility_threshold * (vec4(1.0, 1.0, 1.0, 0.0));

	fColor = (light_sensitivity * (ambient_final + diffuse_final + specular_final)) - threshold;

}