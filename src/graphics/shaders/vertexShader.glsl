#version 440

// Space conversion matrices
uniform mat4 model;         // vertices to world coords
uniform mat4 view;          // conversion to camera coords
uniform mat4 perspective;   // Perspective matrix for fov, aspect etc...

uniform float px;
uniform float py;
uniform float pz;

uniform float thetaX;
uniform float thetaY;
uniform float thetaZ;

uniform float scaleX;
uniform float scaleY;
uniform float scaleZ;

uniform float tick_no;

// Vars in model space
in vec4 vertex_pos_in_model;
in vec4 vertex_normal_in_model;

in vec2 texture_coordinate_in_model;

out vec3 WVp;   // Worldspace Vertex pos
out vec3 WVnN;  // Worldspace vertex normal (Normalized and rotated)
out vec2 texCoord;

mat4 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

void main() {
    mat4 modelRotation = rotationMatrix(vec3(1,0,0), thetaX) * rotationMatrix(vec3(0,1,0), thetaY) * rotationMatrix(vec3(0,0,1), thetaZ);

    WVp = (model * vertex_pos_in_model).xyz;
    WVnN = normalize((modelRotation * vertex_normal_in_model).xyz);

    gl_Position = perspective * view * model * vertex_pos_in_model;
    texCoord = texture_coordinate_in_model;
}
