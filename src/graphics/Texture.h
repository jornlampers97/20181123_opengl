//
// Created by jornl on 28-11-2018.
//

#ifndef UNIVERSE_TEXTURE_H
#define UNIVERSE_TEXTURE_H


#include <GL/glew.h>

class Texture {
public:
    Texture(int width, int height);
    void bind();
    void unbind();
    GLuint getTexture();
    int getWidth();
    int getHeight();
private:
    void setupGraphics();
    GLuint texID;

    GLuint framebufferName = 0;
    GLuint renderedTexture;
    GLuint depthrenderbuffer;

    int width, height;

};


#endif //UNIVERSE_TEXTURE_H
