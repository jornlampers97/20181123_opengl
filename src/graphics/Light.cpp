//
// Created by jorn on 18-11-18.
//

#include "Light.h"

Light::Light(double locX, double locY, double locZ, GLuint shaderID) : Preprocessor(shaderID) {
    this->locX = locX;
    this->locY = locY;
    this->locZ = locZ;

    loc[0] = locX;
    loc[1] = locY;
    loc[2] = locZ;
    loc[3] = 1.0;

    col[0] = 1.0;
    col[1] = 1.0;
    col[2] = 1.0;
    col[3] = 1.0;

    this->intensity = 1000;

    setupGraphics();

}

void Light::setupGraphics() {
    Preprocessor::setupGraphics();
    lightPosID = glGetUniformLocation(shaderID, "light_pos");        // Find the if of uniform var vLight
    lightColID = glGetUniformLocation(shaderID, "light_col");
    lightIntID = glGetUniformLocation(shaderID, "light_intensity");
}

void Light::preprocess() {
    Preprocessor::preprocess();
    glUniform1f(lightIntID, intensity);
    glUniform4fv(lightPosID, 1, loc);
    glUniform4fv(lightColID, 1, col);
}

void Light::setColor(double r, double g, double b, double a) {
    col[0] = r;
    col[1] = g;
    col[2] = b;
    col[3] = a;
}

void Light::displace(GLfloat dx, GLfloat dy, GLfloat dz) {
    loc[0] += dx;
    loc[1] += dy;
    loc[2] += dz;

}

void Light::setIntensity(float intensity) {
    this->intensity = intensity;
}