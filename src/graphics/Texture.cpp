//
// Created by jornl on 28-11-2018.
//

#include <iostream>
#include "Texture.h"

void Texture::setupGraphics() {
    glGenFramebuffers(1, &framebufferName);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);

    glBindTexture(GL_TEXTURE_2D, renderedTexture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glGenRenderbuffers(1, &depthrenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

    // Set "renderedTexture" as our colour attachement #0
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

    // Set the list of draw buffers.
    GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
    // Always check that our framebuffer is ok
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Oei das nie goed chef" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0); // Unbind this texture as buffer, bind screen
}

void Texture::bind() {
    // Render to our framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);
    glViewport(0,0,width,height); // Render on the whole framebuffer, complete from the lower left corner to the upper right
}

void Texture::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Texture::Texture(int width, int height) {
    this->height = height;
    this->width = width;

    setupGraphics();
}

GLuint Texture::getTexture() {
    return renderedTexture;
}

int Texture::getWidth() {
    return width;
}

int Texture::getHeight() {
    return height;
}
