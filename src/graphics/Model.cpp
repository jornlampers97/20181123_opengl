//
// Created by jorn on 24-11-18.
//

#include "Model.h"

#define BUFFER_OFFSET(i) ((char *) NULL + (i))

void Model::render() {
    material->render();

    glUseProgram(shaderID); // explicity assign shader used in frame
    glBindVertexArray(vao); // load correct vao to map gpu memory

    MathHelper::constructModelMatrix(M, posX, posY, posZ, thetaX, thetaY, thetaZ, scaleX, scaleY, scaleZ);
    glUniformMatrix4fv(modelMatrixID, 1, GL_TRUE, M);

    glUniform1f(posXID, posX);
    glUniform1f(posYID, posY);
    glUniform1f(posZID, posZ);

    glUniform1f(scaleXID, scaleX);
    glUniform1f(scaleYID, scaleY);
    glUniform1f(scaleZID, scaleZ);

    glUniform1f(thetaXID, thetaX);
    glUniform1f(thetaYID, thetaY);
    glUniform1f(thetaZID, thetaZ);

    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, NULL);
}

Model::Model(GLuint shaderID, Mesh * mesh, Material * mat){
    this->shaderID = shaderID;
    this->material = mat;
    this->mesh = mesh;

    posX = posY = posZ = 0;
    setupGraphics();
}

void Model::setupGraphics() {

    initMatrices();
    loadMesh();

    // Variables every model instance has
    modelMatrixID = glGetUniformLocation(shaderID, "model");

    posXID =    glGetUniformLocation(shaderID, "px");
    posYID =    glGetUniformLocation(shaderID, "py");
    posZID =    glGetUniformLocation(shaderID, "pz");

    scaleXID =  glGetUniformLocation(shaderID, "scaleX");
    scaleYID =  glGetUniformLocation(shaderID, "scaleY");
    scaleZID =  glGetUniformLocation(shaderID, "scaleZ");

    thetaXID = glGetUniformLocation(shaderID, "thetaX");
    thetaYID = glGetUniformLocation(shaderID, "thetaY");
    thetaZID = glGetUniformLocation(shaderID, "thetaZ");

}

void Model::loadMesh() {
    // These numbers represent the amount of floats/uints that represent normals/inds/verts,
    // NOT the amount of 3d points !!!
    numIndices = mesh->indices.size();
    numVertices = mesh->vertices.size();
    numNormals = mesh->normals.size();
    numUVs = mesh->uvs.size();

    GLfloat vertices[numVertices];
    std::copy(mesh->vertices.begin(), mesh->vertices.end(), vertices);

    GLfloat normals[numNormals];
    std::copy(mesh->normals.begin(), mesh->normals.end(), normals);

    GLfloat uvs[numUVs];
    std::copy(mesh->uvs.begin(), mesh->uvs.end(), uvs);

    GLuint indices[numIndices];
    std::copy(mesh->indices.begin(), mesh->indices.end(), indices);

    // Create the "Remember all" ?
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);

    // Generate buffer space for 6 floats per vertice (x, y, z, nx, ny, nz)
    glBufferData(GL_ARRAY_BUFFER, (numVertices + numNormals + numUVs) * sizeof(GLfloat), NULL, GL_STATIC_DRAW);

    // First half of buffer contains (x, y, z) (vertex pos)
    glBufferSubData(GL_ARRAY_BUFFER, 0,                             numVertices * sizeof(GLfloat), vertices);
    // Second half contains (dx, dy, dz) (directions of normals)
    glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(GLfloat), numNormals * sizeof(GLfloat), normals);

    glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(GLfloat) + numNormals * sizeof(GLfloat), numUVs * sizeof(GLfloat), uvs);

    // Index buffer (indices)
    glGenBuffers(1, &indexBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLuint), indices, GL_STATIC_DRAW);

    // Find the positions of the variables in the shader
    positionID = glGetAttribLocation(shaderID, "vertex_pos_in_model"); // extract s_vPosition variable ID from shader
    normalID = glGetAttribLocation(shaderID, "vertex_normal_in_model");
    uvID = glGetAttribLocation(shaderID, "texture_coordinate_in_model");

    // Tell the shader variables where to find their data
    glVertexAttribPointer(positionID, 3, GL_FLOAT, GL_FALSE, 0, 0); // Pass attribute location so shader can fetch data
    glVertexAttribPointer(normalID, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices))); // Pass attribute location so shader can fetch data
    glVertexAttribPointer(uvID, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices) + sizeof(normals)));

    glEnableVertexAttribArray(positionID); // Enable shader variable
    glEnableVertexAttribArray(normalID); // Enable shader variable
    glEnableVertexAttribArray(uvID);
}

void Model::initMatrices() {
    posX = 0;
    posY = 0;
    posZ = 0;

    thetaX = 0;
    thetaY = 0;
    thetaZ = 0;

    scaleX = 1.0f; // 100 * 1 cm = 1 m
    scaleY = 1.0f; // 100 * 1 cm = 1 m
    scaleZ = 1.0f; // 100 * 1 cm = 1 m

    transMatrix     = new GLfloat[16]; MathHelper::makeIdentity(transMatrix);
    scaleMatrix     = new GLfloat[16]; MathHelper::makeIdentity(scaleMatrix);
    M               = new GLfloat[16]; MathHelper::makeIdentity(M);
}

void Model::displace(double dX, double dY, double dZ) {
    this->posX += dX;
    this->posY += dY;
    this->posZ += dZ;
}

void Model::setScale(float scaleX, float scaleY, float scaleZ) {
    this->scaleX = scaleX;
    this->scaleY = scaleY;
    this->scaleZ = scaleZ;
}

void Model::setScale(float scale) {
    scaleX = scaleY = scaleZ = scale;
}

void Model::rotate(float rx, float ry, float rz) {
    thetaX += rx;
    thetaY += ry;
    thetaZ += rz;
}

vec3 Model::getLocation() {
    return vec3(posX, posY, posZ);
}
