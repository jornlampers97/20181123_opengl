//
// Created by jorn on 12-12-18.
//

#ifndef SHADERS_POSTPROCESSOR_H
#define SHADERS_POSTPROCESSOR_H


#include <GL/glew.h>

class Postprocessor {
public:
    Postprocessor(GLuint shaderID);
    virtual void postprocess();
protected:
    virtual void setupGraphics();
    GLuint shaderID;
    GLuint vao;

};


#endif //SHADERS_POSTPROCESSOR_H
