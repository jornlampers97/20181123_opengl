//
// Created by jorn on 18-11-18.
//

#ifndef SHADERS_SHADER_H
#define SHADERS_SHADER_H

#include <GL/glew.h>

class Shader {
public:
    Shader(char * vertexShaderSourceCode, char * fragmentShaderSourceCode);
    GLuint getID();
    GLuint getVertexShaderID();
    GLuint getFragmentShaderID();
    bool compiledStatus(GLint shaderID);
private:
    GLuint makeVertexShader(const char * shaderSource);
    GLuint makeFragmentShader(const char * shaderSource);
    GLuint makeShaderProgram(GLuint vertexShaderID, GLuint fragmentShaderID);
    char * readFile(char * filename);
    GLuint shaderID;
    GLuint vertexShaderID;
    GLuint fragmentShaderID;
    bool vertexCompiled;
    bool fragmentCompiled;
};


#endif //SHADERS_SHADER_H
