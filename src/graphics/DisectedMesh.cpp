//
// Created by jornl on 25-11-2018.
//

#include "DisectedMesh.h"

Mesh * DisectedMesh::construct() {
    Mesh * out = new Mesh();
    for(int v = 0; v < vertices.size(); v++) {
        out->vertices.push_back(vertices[v].x);
        out->vertices.push_back(vertices[v].y);
        out->vertices.push_back(vertices[v].z);
    }

    for(int n = 0; n < normals.size(); n++) {
        out->normals.push_back(normals[n].x);
        out->normals.push_back(normals[n].y);
        out->normals.push_back(normals[n].z);
    }

    for(int u = 0; u < uvs.size(); u++) {
        out->uvs.push_back(uvs[u].x);
        out->uvs.push_back(uvs[u].y);
    }

    for(int i = 0; i < indices.size(); i++) {
        out->indices.push_back(indices[i].a);
        out->indices.push_back(indices[i].b);
        out->indices.push_back(indices[i].c);
    }
    return out;
}
