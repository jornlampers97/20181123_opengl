//
// Created by jorn on 12-12-18.
//

#include "Postprocessor.h"

Postprocessor::Postprocessor(GLuint shaderID) {
    this->shaderID = shaderID;
}

void Postprocessor::setupGraphics() {
    glUseProgram(shaderID); // explicity assign shader used in frame
    // Create the "Remember all" ?
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
}

void Postprocessor::postprocess() {
    glBindVertexArray(vao);
}