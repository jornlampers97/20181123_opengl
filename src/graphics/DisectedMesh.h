//
// Created by jornl on 25-11-2018.
//

#ifndef UNIVERSE_DISECTEDMESH_H
#define UNIVERSE_DISECTEDMESH_H


#include <GL/glew.h>
#include <vector>
#include "../util/vec3.h"
#include "../util/ind3.h"
#include "Mesh.h"
#include "../util/vec2.h"

class DisectedMesh {
public:
    std::vector<vec3> vertices;
    std::vector<vec3> normals;
    std::vector<vec2> uvs;
    std::vector<ind3> indices;

    Mesh * construct();
};


#endif //UNIVERSE_DISECTEDMESH_H
