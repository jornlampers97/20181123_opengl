//
// Created by jorn on 24-11-18.
//

#ifndef SHADERS_OBJECT_H
#define SHADERS_OBJECT_H


#include <GL/glew.h>
#include "../util/MathHelper.h"
#include "Material.h"
#include "Mesh.h"
#include "../util/vec3.h"

class Model {

public:
    Model(GLuint shaderID, Mesh * mesh, Material * mat);
    void render();
    void displace(double dX, double dY, double dZ);
    void setScale(float scaleX, float scaleY, float scaleZ);
    void setScale(float scale);
    void rotate(float rx, float ry, float rz);
    vec3 getLocation();

protected:
    GLfloat posX, posY, posZ;
    GLfloat thetaX, thetaY, thetaZ;
    GLfloat scaleX, scaleY, scaleZ;

private:
    void setupGraphics();
    void initMatrices();
    void loadMesh();

    // These nums represent the amount of floats/uints that represent normals/inds/verts, NOT the amount of 3d points !!!
    int numIndices, numVertices, numNormals, numUVs;

    GLuint shaderID;

    GLuint vao;// = 0;
    GLuint vertexBufferID;
    GLuint positionID, normalID, uvID;
    GLuint indexBufferID;
    GLint posXID, posYID, posZID;
    GLint scaleXID, scaleYID, scaleZID;
    GLint thetaXID, thetaYID, thetaZID;
    GLuint modelMatrixID;

    GLfloat * transMatrix;
    GLfloat * scaleMatrix;

    GLfloat * M;

    Material * material;
    Mesh * mesh;

};


#endif //SHADERS_OBJECT_H
