//
// Created by jorn on 24-11-18.
//

#ifndef SHADERS_MESH_H
#define SHADERS_MESH_H


#include <vector>
#include <GL/glew.h>

class Mesh {
public:
    Mesh();
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> uvs;
    std::vector<GLuint> indices;
    void integrate(Mesh * m);
};


#endif //SHADERS_MESH_H
