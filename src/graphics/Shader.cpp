//
// Created by jorn on 18-11-18.
//

#include <cstdio>
#include <cstdlib>
#include "Shader.h"

Shader::Shader(char *vertexShaderSourceCode, char *fragmentShaderSourceCode) {
    vertexShaderID = makeVertexShader(readFile(vertexShaderSourceCode));
    fragmentShaderID = makeFragmentShader(readFile(fragmentShaderSourceCode));
    shaderID = makeShaderProgram(vertexShaderID, fragmentShaderID);
}

GLuint Shader::getVertexShaderID() {
    return vertexShaderID;
}

GLuint Shader::getFragmentShaderID() {
    return fragmentShaderID;
}

GLuint Shader::makeVertexShader(const char * shaderSource) {
    GLuint ID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource (ID, 1, &shaderSource, NULL);
    glCompileShader(ID);
    vertexCompiled = compiledStatus(ID);
    if (vertexCompiled) {
        return ID;
    }
    return -1;
}

GLuint Shader::makeFragmentShader(const char * shaderSource) {
    GLuint ID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(ID, 1, &shaderSource, NULL);
    glCompileShader(ID);
    fragmentCompiled = compiledStatus(ID);
    if (fragmentCompiled) {
        return ID;
    }
    return -1;
}

bool Shader::compiledStatus(GLint shaderID){
    GLint compiled = 0;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
    if (compiled) {
        return true;
    }
    else {
        GLint logLength;
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
        char* msgBuffer = new char[logLength];
        glGetShaderInfoLog(shaderID, logLength, NULL, msgBuffer);
        printf ("%s\n", msgBuffer);
        delete (msgBuffer);
        return false;
    }
}

GLuint Shader::makeShaderProgram(GLuint vertexShaderID, GLuint fragmentShaderID) {
    GLuint ID = glCreateProgram();
    glAttachShader(ID, vertexShaderID);
    glAttachShader(ID, fragmentShaderID);
    glLinkProgram(ID);
    return ID;
}

GLuint Shader::getID() {
    return shaderID;
}

char * Shader::readFile(char * filename) {

    FILE *fp;
    char *content = NULL;

    int count=0;

    if (filename != NULL) {
        fp = fopen(filename,"rt");

        if (fp != NULL) {

            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);

            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count+1));
                count = fread(content,sizeof(char),count,fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;

}