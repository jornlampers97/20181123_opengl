//
// Created by jorn on 23-11-18.
//

#ifndef SHADERS_MATERIAL_H
#define SHADERS_MATERIAL_H


#include <GL/glew.h>

class Material {
private:
    void setupGraphics();

    GLuint shaderID;
    GLint ambientID, diffuseID, specularID;
    GLint ambientMapID, diffuseMapID, specularMapID;
    GLint ambientIntensityID, diffuseIntensityID, specularIntensityID;
    GLint hardnessID;

    GLfloat color_ambient[4] = {1.0, 1.0, 1.0, 1.0};
    GLfloat color_diffuse[4] = {1.0, 1.0, 1.0, 1.0};
    GLfloat color_specular[4] = {1.0, 1.0, 1.0, 1.0};

    GLfloat ambient_intensity, diffuse_intensity, specular_intensity;
    GLfloat hardness;

    GLuint texture_ambient, texture_diffuse, texture_specular;

public:
    Material(GLuint shaderID);

    void setAmbientColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
    void setDiffuseColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
    void setSpecularColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

    void setAmbientTexture(GLuint texID);
    void setDiffuseTexture(GLuint texID);
    void setSpecularTexture(GLuint texID);

    void setAmbientIntensity(float intensity);
    void setDiffuseIntensity(float intensity);
    void setSpecularIntensity(float intensity);

    void setHardness(GLfloat intensity);

    void render();

};


#endif //SHADERS_MATERIAL_H
