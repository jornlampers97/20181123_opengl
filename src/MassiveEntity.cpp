//
// Created by jornl on 3-12-2018.
//

#include "MassiveEntity.h"

MassiveEntity::MassiveEntity(GLuint shaderID, Mesh *mesh, Material *mat, double mass) : Entity(shaderID, mesh, mat, mass) {

}

void MassiveEntity::tick(long micros) {
    Entity::tick(micros);
    double secs = (double) micros / 1000000;

    // Calculate force of gravity, apply to all objects within reasonable range
    //this->rotate(0, secs, 0);
}
