//
// Created by jorn on 18-11-18.
//

#ifndef SHADERS_CAMERA_H
#define SHADERS_CAMERA_H


#include <GL/glew.h>
#include "util/vec3.h"
#include "graphics/Model.h"
#include "graphics/Preprocessor.h"
#include "sound/Ear.h"

class Camera : public Preprocessor, public Ear {
public:
    Camera(GLuint shaderID);

    void preprocess();
    void setupGraphics();

    void hear(Sound * s);

    double getX();
    double getY();
    double getZ();
    double getPitch();
    double getYaw();
    double getRoll();

    void reset();
    void displace(vec3 mov);
    void addPitch(double p);
    void addYaw(double y);
    void addRoll(double r);
    void setLightSensitivity(double s);
    void setLightThreshold(double t);
    void setResolution(int resX, int resY);

    void setReferenceFrame(Model * ref);

private:
    void initMatrices();

    int resX, resY;

    double x, y, z;
    double pitch, yaw, roll; // In radians
    double lightVisibilityThreshold, lightSensitivity;

    GLuint viewMatrixID;
    GLuint perspectiveMatrixID;
    GLint camXID, camYID, camZID;
    GLint lightVisibilityThresholdID, lightSensitivityID;

    Model * referenceFrame;

    GLfloat * rotXMatrix;
    GLfloat * rotYMatrix;
    GLfloat * rotZMatrix;
    GLfloat * allRotsMatrix;

    GLfloat * transMatrix;
    GLfloat * tempMatrix1;

    GLfloat * P;
    GLfloat * V;

};


#endif //SHADERS_CAMERA_H
