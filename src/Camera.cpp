//
// Created by jorn on 18-11-18.
//

#include "Camera.h"
#include "util/MathHelper.h"

Camera::Camera(GLuint shaderID) : Preprocessor(shaderID) {
    pitch = yaw = roll = 0.0;
    x = y = z = 0.0;
    lightVisibilityThreshold = 0.0;
    lightSensitivity = 1.0;

    referenceFrame = nullptr;

    setupGraphics();
}

double Camera::getX() {
    return x;
}

double Camera::getY() {
    return y;
}

double Camera::getZ() {
    return z;
}

double Camera::getPitch() {
    return pitch;
}

double Camera::getYaw() {
    return yaw;
}

double Camera::getRoll() {
    return roll;
}

void Camera::reset() {
    pitch = yaw = roll = 0.0;
    x = y = z = 0.0;
}

void Camera::displace(vec3 mov) {
    x+=mov.x;
    y+=mov.y;
    z+=mov.z;
}

void Camera::setupGraphics() {
    perspectiveMatrixID = glGetUniformLocation(shaderID, "perspective");
    viewMatrixID = glGetUniformLocation(shaderID, "view");

    camXID = glGetUniformLocation(shaderID, "camX");
    camYID = glGetUniformLocation(shaderID, "camY");
    camZID = glGetUniformLocation(shaderID, "camZ");

    lightVisibilityThresholdID = glGetUniformLocation(shaderID, "light_visibility_threshold");
    lightSensitivityID = glGetUniformLocation(shaderID, "light_sensitivity");

    initMatrices();
}

void Camera::initMatrices() {
    P               = new GLfloat[16]; MathHelper::makeIdentity(P);
    V               = new GLfloat[16]; MathHelper::makeIdentity(V);

    MathHelper::makePerspectiveMatrix(P, 55.0f, (float)resX/resY, 1.0f, 100.0f);
}

void Camera::preprocess() {
    // push view matrix to GPU
    // Change the view matrix according to pitch yaw roll and campos
    rotXMatrix      = new GLfloat[16]; MathHelper::makeIdentity(rotXMatrix);
    rotYMatrix      = new GLfloat[16]; MathHelper::makeIdentity(rotYMatrix);
    rotZMatrix      = new GLfloat[16]; MathHelper::makeIdentity(rotZMatrix);
    tempMatrix1     = new GLfloat[16]; MathHelper::makeIdentity(tempMatrix1);

    transMatrix     = new GLfloat[16]; MathHelper::makeIdentity(transMatrix);
    allRotsMatrix   = new GLfloat[16]; MathHelper::makeIdentity(allRotsMatrix);
    V               = new GLfloat[16]; MathHelper::makeIdentity(V);

    vec3 refLoc = vec3(0,0,0);
    if(referenceFrame != nullptr) refLoc = referenceFrame->getLocation();

    // Update view matrix
    MathHelper::makeRotateX(rotXMatrix, pitch);
    MathHelper::makeRotateY(rotYMatrix, yaw);
    MathHelper::makeRotateZ(rotZMatrix, roll);

    MathHelper::makeTranslate(transMatrix, (-x - refLoc.x), (-y - refLoc.y), (-z - refLoc.z));

    MathHelper::matrixMult4x4(tempMatrix1, rotYMatrix, rotZMatrix); // Rotate Z, then rotate Y...
    MathHelper::matrixMult4x4(allRotsMatrix, rotXMatrix, tempMatrix1); // then rotate X

    //MathHelper::copyMatrix(transMatrix, V);
    MathHelper::matrixMult4x4(V, allRotsMatrix, transMatrix);      // update view matrix

    glUniformMatrix4fv(perspectiveMatrixID, 1, GL_TRUE, P);
    glUniformMatrix4fv(viewMatrixID, 1, GL_TRUE, V);            // push view matrix

    glUniform1f(camXID, (x + refLoc.x));
    glUniform1f(camYID, (y + refLoc.y));
    glUniform1f(camZID, (z + refLoc.z));

    glUniform1f(lightVisibilityThresholdID, lightVisibilityThreshold);
    glUniform1f(lightSensitivityID, lightSensitivity);

}

void Camera::addPitch(double p) {
    this->pitch += p;

    if(pitch > 0.5 * M_PI)
        pitch = 0.5 * M_PI;

    if(pitch < -0.5 * M_PI)
        pitch = -0.5 * M_PI;
}

void Camera::addYaw(double y) {
    this->yaw += y;
}

void Camera::addRoll(double r) {
    this->roll += r;
}

void Camera::setLightSensitivity(double s) {
    lightSensitivity = s;
}

void Camera::setLightThreshold(double t) {
    lightVisibilityThreshold = t;
}

void Camera::setResolution(int resX, int resY) {
    this->resX = resX;
    this->resY = resY;
    initMatrices();
}

void Camera::setReferenceFrame(Model *ref) {
    this->referenceFrame = ref;
}

void Camera::hear(Sound *s) {
    Ear::hear(s);
}

