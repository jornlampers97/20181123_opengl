#include "vec3.h"

#include <cmath>
#include <cfloat>

vec3::vec3() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

vec3::vec3(double x, double y, double z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

double vec3::dot(vec3 v) {
    return this->x * v.x + this->y * v.y + this->z * v.z;
}

vec3 vec3::multiply(double m) {
    return {this->x*m, this->y*m, this->z*m};
}

vec3 vec3::cross(vec3 v) {
    return
    {
        this->y*v.z-v.y*this->z,
        -(this->x*v.z-v.x*this->z),
        this->x*v.y-v.x*this->y
    };
}

double vec3::magnitude() {
    return sqrt(x*x+y*y+z*z);
}

vec3 vec3::exponential(double p) {
    return normalize().multiply(pow(magnitude(), p));
}

vec3 vec3::normalize() {
    if(magnitude() == 0) return {0,0,0};
    return multiply(1/magnitude());
}

vec3 vec3::add(vec3 v) {
    return {x + v.x, y + v.y, z + v.z};
}

vec3 vec3::inverse() {
    return {-this->x, -this->y, -this->z};
}

vec3 vec3::rotateX(double rads) {
    return
            {
                    this->x,
                    this->y*cos(rads) - this->z*sin(rads),
                    this->y*sin(rads) + this->z*cos(rads)
            };
}

vec3 vec3::rotateY(double rads) {
    return
            {
                    this->x*cos(rads) + this->z*sin(rads),
                    this->y,
                    -this->x*sin(rads) + this->z*cos(rads)
            };
}

vec3 vec3::rotateZ(double rads) {
    return
            {
                    this->x*cos(rads) - this->y*sin(rads),
                    this->x*sin(rads) + this->y*cos(rads),
                    this->z
            };
}

// Source: https://stackoverflow.com/questions/7582398/rotate-a-vector-about-another-vector
void make_quat (double quat[4], const vec3 & v2, double angle)
{
    // there's no reason you can't use 'doubles' for angle, etc.
    // there's not much point in applying a rotation outside of [-PI, +PI];
    // as that covers the practical 2.PI range.

    // any time graphics / floating point overlap, we have to think hard
    // about degenerate cases that can arise quite naturally (think of
    // pathological cancellation errors that are *possible* in seemingly
    // benign operations like inner products - and other running sums).

    vec3 axis (v2);

    double rl = sqrt(axis.dot(axis));
    if (rl < FLT_EPSILON) // we'll handle this as no rotation:
    {
        quat[0] = 0.0, quat[1] = 0.0, quat[2] = 0.0, quat[3] = 1.0;
        return; // the 'identity' unit quaternion.
    }

    double ca = cos(angle);

    // we know a maths library is never going to yield a value outside
    // of [-1.0, +1.0] right? Well, maybe we're using something else -
    // like an approximating polynomial, or a faster hack that's a little
    // rough 'around the edge' cases? let's *ensure* a clamped range:
    ca = (ca < -1.0f) ? -1.0f : ((ca > +1.0f) ? +1.0f : ca);

    // now we find cos / sin of a half-angle. we can use a faster identity
    // for this, secure in the knowledge that 'sqrt' will be valid....

    double cq = sqrt((1.0f + ca) / 2.0f); // cos(acos(ca) / 2.0);
    double sq = sqrt((1.0f - ca) / 2.0f); // sin(acos(ca) / 2.0);

    axis = axis.multiply( sq / rl); // i.e., scaling each element, and finally:

    quat[0] = axis.x, quat[1] = axis.y, quat[2] = axis.z, quat[3] = cq;

    return;
}

// Source: https://stackoverflow.com/questions/7582398/rotate-a-vector-about-another-vector
static inline void
qmul (double r[4], const double q[4], const double p[4])
{
    // quaternion multiplication: r = q * p

    double w0 = q[3], w1 = p[3];
    double x0 = q[0], x1 = p[0];
    double y0 = q[1], y1 = p[1];
    double z0 = q[2], z1 = p[2];

    r[3] = w0 * w1 - x0 * x1 - y0 * y1 - z0 * z1;
    r[0] = w0 * x1 + x0 * w1 + y0 * z1 - z0 * y1;
    r[1] = w0 * y1 + y0 * w1 + z0 * x1 - x0 * z1;
    r[2] = w0 * z1 + z0 * w1 + x0 * y1 - y0 * x1;
}

// Source: https://stackoverflow.com/questions/7582398/rotate-a-vector-about-another-vector
vec3 vec3::rotate(vec3 axis, double rads) {
    // 3D vector rotation: v = q * v * conj(q)

    double q[4]; // we want to find the unit quaternion for `v2` and `A`...

    make_quat(q, axis, rads);

    // what about `v1`? can we access elements with `operator [] (int)` (?)
    // if so, let's assume the memory: `v1[0] .. v1[2]` is contiguous.
    // you can figure out how you want to store and manage your vec3 class.

    double r[4], p[4];

    r[0] = + this->x, r[1] = + this->y, r[2] = + this->z, r[3] = +0.0;
    qmul(r, q, r);

    p[0] = - q[0], p[1] = - q[1], p[2] = - q[2], p[3] = q[3];
    qmul(r, r, p);

    return vec3(r[0], r[1], r[2]);

    // `v1` has been rotated by `(A)` radians about the direction vector `v2` ...

}

vec3 vec3::operator+(vec3 v) {
    return this->add(v);
}

vec3 vec3::operator-(vec3 v) {
    return this->add(v.inverse());
}

vec3 vec3::operator*(vec3 v) {
    return this->cross(v);
}

vec3 vec3::operator*(double d) {
    return this->multiply(d);
}

vec3 vec3::operator/(double d) {
    return this->multiply(1.0/d);
}
