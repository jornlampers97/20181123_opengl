//
// Created by jorn on 11-12-18.
//

#ifndef SHADERS_VEC2_H
#define SHADERS_VEC2_H


class vec2 {
public:
    vec2(double x, double y);
    double x, y;

};


#endif //SHADERS_VEC2_H
