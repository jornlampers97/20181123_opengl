//
// Created by jorn on 23-12-18.
//

#include "Heightmap.h"
#include "SimplexNoise.h"

Heightmap::Heightmap(int seed) {
    this->seed = seed;
    noiseGenerator = SimplexNoise(0.03, seed, 2.0, 0.5);
}

float Heightmap::getHeight(float x, float y) {
    return noiseGenerator.fractal(2,x,y);
}
