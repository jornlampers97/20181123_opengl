//
// Created by jornl on 25-11-2018.
//

#include "ind3.h"

ind3::ind3(GLuint a, GLuint b, GLuint c) {
    this->a = a;
    this->b = b;
    this->c = c;
}

ind3 ind3::add(int i) {
    return ind3(this->a+i, this->b+i, this->c+i);
}

ind3 ind3::invert() {
    return ind3(this->c, this->b, this->a);
}

ind3 ind3::add(ind3 i) {
    return ind3(this->a+i.a, this->b+i.b, this->c+i.c);
}
