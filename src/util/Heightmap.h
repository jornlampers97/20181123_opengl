//
// Created by jorn on 23-12-18.
//

#ifndef SHADERS_HEIGHTMAP_H
#define SHADERS_HEIGHTMAP_H


#include "SimplexNoise.h"

class Heightmap {
public:
    Heightmap(int seed);
    float getHeight(float x, float y);

private:
    int seed;
    SimplexNoise noiseGenerator;

};


#endif //SHADERS_HEIGHTMAP_H
