//
// Created by jorn on 27-10-18.
//

#ifndef UNIVERSE_VEC3_H
#define UNIVERSE_VEC3_H

class vec3 {
public:
    double x;
    double y;
    double z;

    vec3();
    vec3(double x, double y, double z);

    vec3 multiply(double m);
    vec3 add(vec3 v);
    vec3 cross(vec3 v);
    vec3 normalize();
    vec3 inverse();
    vec3 exponential(double pow);
    vec3 rotateX(double rads);
    vec3 rotateY(double rads);
    vec3 rotateZ(double rads);
    vec3 rotate(vec3 axis, double rads); // TODO: Untested

    double magnitude();
    double dot(vec3 v);

    vec3 operator + (vec3 v);
    vec3 operator - (vec3 v);
    vec3 operator * (vec3 v);
    vec3 operator * (double d);
    vec3 operator / (double d);

};


#endif //UNIVERSE_VEC3_H
