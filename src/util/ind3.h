//
// Created by jornl on 25-11-2018.
//

#ifndef UNIVERSE_IND3_H
#define UNIVERSE_IND3_H

#include <GL/glew.h>

class ind3 {
public:
    GLuint a;
    GLuint b;
    GLuint c;

    ind3(GLuint a, GLuint b, GLuint c);

    ind3 add(ind3 i);
    ind3 add(int i);

    ind3 invert();

    ind3 operator + (ind3 i);
    ind3 operator - (ind3 i);
};


#endif //UNIVERSE_IND3_H
