//
// Created by jorn on 18-11-18.
//

#include "Scene.h"
#include "graphics/Postprocessor.h"

Scene::Scene(GLuint shaderID) {
    this->shaderID = shaderID;

    preprocessors = std::vector<Preprocessor *>();
    postprocessors = std::vector<Postprocessor *>();
    models = std::vector<Model *>();
    entities = std::vector<Entity *>();

    init();
}

Model * sun;

void Scene::init() {

    setupGraphics();
    vec3 lightPos = vec3(-10, 30, 10);

    light = new Light(lightPos.x, lightPos.y, lightPos.z, shaderID);
    light->setColor(1, 1, 1, 1.0);
    light->setIntensity(1);
    this->addLight(light);

    Material * matTerrain = new Material(shaderID);
    matTerrain->setAmbientColor(0.0f, 0.0f, 0.0f, 1.0f);
    matTerrain->setDiffuseColor(0.0f, 0.0f, 0.0f, 1.0f);
    matTerrain->setSpecularColor(0.0f, 0.0f, 0.0f, 1.0f);
    matTerrain->setHardness(3);

    /* load an image file directly as a new OpenGL texture  (x3) */
   GLuint tex_amb = SOIL_load_OGL_texture
            (
                    "tex1.png",
                    SOIL_LOAD_AUTO,
                    SOIL_CREATE_NEW_ID,
                    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
            );
    matTerrain->setAmbientTexture(tex_amb);
    matTerrain->setAmbientIntensity(0.1);

    GLuint tex_diff = SOIL_load_OGL_texture
            (
                    "sand.png",
                    SOIL_LOAD_AUTO,
                    SOIL_CREATE_NEW_ID,
                    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
            );
    matTerrain->setDiffuseTexture(tex_diff);
    matTerrain->setDiffuseIntensity(0.5);

    GLuint tex_spec = SOIL_load_OGL_texture
            (
                    "glitter.png",
                    SOIL_LOAD_AUTO,
                    SOIL_CREATE_NEW_ID,
                    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
            );
    matTerrain->setSpecularTexture(tex_spec);
    matTerrain->setSpecularIntensity(1.0);
/*
    Mesh * sunMesh = MeshBuilder::constructSphere(10,10);
    Material * sunMat = new Material(shaderID);
    sunMat->setAmbientColor((GLfloat)1.0, (GLfloat)1.0, (GLfloat)1.0, (GLfloat) 1.0);
    sunMat->setAmbientIntensity(1.0);
    sun = new Model(shaderID, sunMesh, sunMat);
    sun->displace(lightPos.x, lightPos.y, lightPos.z);
    sun->setScale(5);
    addModel(sun);
*/
    camera = new Camera(shaderID);
    camera->displace(vec3(0, 5, 0));
    camera->setLightSensitivity(1.0);
    camera->setLightThreshold(0.0);
    this->setCamera(camera);

    float rasterSize = 128.0;

    int tilesRadius = 1;

    for(int x = 0-tilesRadius; x < 1+tilesRadius; x++) {
        for(int z = 0-tilesRadius; z < 1+tilesRadius; z++) {
            Mesh * rasterMesh = MeshBuilder::constructTerrainRaster(rasterSize,rasterSize, x*rasterSize, z*rasterSize);
            Model * ob = new Model(shaderID, rasterMesh, matTerrain);
            ob->setScale(rasterSize,1,rasterSize);
            ob->displace(x*(rasterSize+0), 0, z * (rasterSize+0));
            addModel(ob);
        }
    }


    Entity * cube = new Entity(shaderID, MeshBuilder::constructCube(), matTerrain);
    cube->displace(0, 1, 0);
    cube->setScale(1);
    //addEntity(cube);

}

void Scene::setupGraphics() {
    glUseProgram(shaderID); // explicity assign shader used in frame
    // Create the "Remember all" ?
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    tickNoID = glGetUniformLocation(shaderID, "tick_no");
}

void Scene::addEntity(Entity * o) {
    entities.push_back(o);
}

void Scene::addModel(Model * m) {
    models.push_back(m);
}

void Scene::addLight(Light * l) {
    preprocessors.push_back(l);
}

Camera * Scene::getCamera() {
    return camera;
}

void Scene::setCamera(Camera * camera) {
    this->camera = camera;
    preprocessors.push_back(camera);
}

Input * Scene::getControls() {
    return controls;
}

void Scene::setControls(Input * in) {
    this->controls = in;
}

void Scene::setSimSpeed(double simspeed) {
    this->simSpeed = simspeed;
}

void Scene::preprocess() {
    glBindVertexArray(vao);
    glUniform1f(tickNoID, tickNo);

    light->displace(0.3,0,0);
    //sun->displace(0.3, 0, 0);

    if(controls != nullptr) {
        vec3 movDir = controls->moveDirection().rotateX(camera->getPitch()).rotateY(camera->getYaw()).rotateZ(camera->getRoll());
        camera->displace(movDir);
    }
}

void Scene::tick(long micros) {

    double dt = (double) micros / 1000000.0 * simSpeed;
    for(Entity * e : entities) e->tick(micros * simSpeed);

    tickNo++;
}

double Scene::getSimSpeed() {
    return simSpeed;
}

int Scene::getTickNo() {
    return tickNo;
}
