//
// Created by jornl on 3-12-2018.
//

#ifndef SHADERS_MASSIVEENTITY_H
#define SHADERS_MASSIVEENTITY_H


#include "Entity.h"

class MassiveEntity : public Entity {

public:
    MassiveEntity(GLuint shaderID, Mesh * mesh, Material * mat, double mass);
    void tick(long micros);
};


#endif //SHADERS_MASSIVEENTITY_H
