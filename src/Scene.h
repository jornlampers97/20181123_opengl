//
// Created by jorn on 18-11-18.
//

#ifndef SHADERS_SCENE_H
#define SHADERS_SCENE_H

#include <vector>
#include "graphics/Light.h"
#include "Camera.h"
#include "graphics/Model.h"
#include "userinput/Input.h"
#include "Entity.h"
#include <iostream>
#include "graphics/Model.h"
#include "graphics/MeshBuilder.h"
#include "MassiveEntity.h"

#include "include/SOIL.h"
#include "graphics/Postprocessor.h"

class Scene {
public:
    Scene(GLuint shaderID);
    void preprocess();
    void addEntity(Entity *o);
    void addModel(Model *m);
    void addLight(Light * l);
    void setCamera(Camera * c);
    void setupGraphics();
    void setSimSpeed(double simspeed);
    double getSimSpeed();
    int getTickNo();

    std::vector<Preprocessor *> preprocessors;
    std::vector<Postprocessor *> postprocessors;
    std::vector<Model *> models;
    std::vector<Entity *> entities;

    Camera * getCamera();
    void tick(long micros);

    Input * getControls();
    void setControls(Input * in);

private:
    const double G = 6.674 * pow(10, -11); // (N/m/kg) https://en.wikipedia.org/wiki/Gravitational_constant

    Camera * camera;

    Input * controls;

    GLuint shaderID;
    GLuint vao;
    GLuint tickNoID;

    float tickNo = 0;
    float simSpeed = 864000.0;

    void init();

    Light * light;

};


#endif //SHADERS_SCENE_H
