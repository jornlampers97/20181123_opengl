#include <GL/glew.h>
#include <GL/freeglut.h>
#include <chrono>
#include <iostream>

#include "graphics/Shader.h"
#include "userinput/Settings.h"
#include "userinput/Input.h"
#include "Scene.h"
#include "graphics/Texture.h"

std::chrono::high_resolution_clock::time_point startTime;
std::chrono::high_resolution_clock::time_point lastFrameTime;
std::chrono::high_resolution_clock::time_point lastTickTime;

Shader * shader;
Scene * scene;
Input * input;
Settings * settings;

Texture * frameBuffer;

int frame = 0;

long timeSinceLastFrame() {
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    long elapsed = std::chrono::duration_cast< std::chrono::microseconds>(now - lastFrameTime).count();
    return elapsed;
}

long timeSinceLastTick() {
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    long elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - lastTickTime).count();
    return elapsed;
}

void updateLastFrameTime() {
    lastFrameTime = std::chrono::high_resolution_clock::now();
}

void updateLastTickTime() {
    lastTickTime = std::chrono::high_resolution_clock::now();
}

void reshape(int w, int h) {
    glViewport(0,0,w,h);
}

void tick(long elapsed) {
    scene->tick(elapsed);
}

void idle() {
    if(timeSinceLastFrame() >= settings->FRAME_TIME * 1000000) {
        glutPostRedisplay();
        updateLastFrameTime();
    }

    long elapsed = timeSinceLastTick();
    updateLastTickTime();
    tick(elapsed);
}

void mouse_move(int x, int y) {

}

void mouse_drag(int x, int y) {
    int resX = settings->RESOLUTION_X;
    int resY = settings->RESOLUTION_Y;

    if(x!=resX/2 || y!=resY/2) { // If cusor has moved from center of screen
        glutWarpPointer(resX / 2, resY / 2); // Move cursor back to center of screen

        int dx = resX/2-x; // yaw    ->     left    <->     right
        int dy = resY/2-y; // pitch  ->      up     <->      down

        input->mouseMove(dx*settings->MOUSE_SENSITIVITY, dy*settings->MOUSE_SENSITIVITY);
    }
}

void keyDown(unsigned char key, int x, int y) {
    input->keyBoardDown(key);
    if(key == 'f') {
        glutFullScreen();
        //glutSetCursor(GLUT_CURSOR_NONE);
        settings->RESOLUTION_X = 2560;
        settings->RESOLUTION_Y = 1440;

        scene->getCamera()->setResolution(settings->RESOLUTION_X, settings->RESOLUTION_Y);

    }
}

void keyUp(unsigned char key, int x, int y) {
    input->keyBoardUp(key);
}

void render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Draw scene to framebuffer
    //frameBuffer->bind();
    scene->preprocess();
    for(Preprocessor * pp : scene->preprocessors)
        pp->preprocess();
    for(Model * object : scene->models)
        object->render();
    for(Entity * entity : scene->entities)
        entity->render();
    for(Postprocessor * pp : scene->postprocessors)
        pp->postprocess();
    //frameBuffer->unbind();

    glutSwapBuffers();
    frame++;
}

long timeElapsed() { // microseconds
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    long elapsed = std::chrono::duration_cast< std::chrono::microseconds>(now - startTime).count();
    return elapsed;
}

int main(int argc, char** argv) {

    settings = new Settings();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(settings->RESOLUTION_X, settings->RESOLUTION_Y);
    glutCreateWindow("Mooi man");

    glutReshapeFunc(reshape);
    glutDisplayFunc(render);
    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);
    glutPassiveMotionFunc(mouse_move);
    glutMotionFunc(mouse_drag);


    glutIdleFunc(idle);
    glewInit();

    shader = new Shader("vertexShader.glsl", "fragmentShader.glsl");
    scene = new Scene(shader->getID());
    scene->getCamera()->setResolution(settings->RESOLUTION_X, settings->RESOLUTION_Y);

    frameBuffer = new Texture(settings->RESOLUTION_X, settings->RESOLUTION_Y);

    input = new Input(scene->getCamera(), settings);

    scene->setControls(input);

    glEnable(GL_DEPTH_TEST);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    //glEnable(GL_CULL_FACE); // Dont bind the backside of triangles (inside of 3d objects)
    //glCullFace(GL_BACK); // Back of a vertex shouldnt be rendered
    //glPolygonMode(GL_BACK, GL_LINE);

    startTime = std::chrono::high_resolution_clock::now();
    updateLastFrameTime();
    updateLastTickTime();

    glutMainLoop();

    return 0;
}