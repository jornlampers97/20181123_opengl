//
// Created by jornl on 3-12-2018.
//

#ifndef SHADERS_ENTITY_H
#define SHADERS_ENTITY_H


#include "physics/Tickable.h"
#include "graphics/Model.h"
#include "util/vec3.h"

class Entity : public Tickable, public Model {
public:
    Entity(GLuint shaderID, Mesh * mesh, Material * mat);
    Entity(GLuint shaderID, Mesh * mesh, Material * mat, double mass);

    void tick(long micros);
    void addVelocity(vec3 v);
    void applyForce(vec3 f);
    void setVelocity(vec3 v);
    void setMass(double mass);
    void addMass(double mass);

    vec3 getVelocity();
    double getMass();
private:
    vec3 velocity;  // Meters/second
    double mass;    // Kilograms
};


#endif //SHADERS_ENTITY_H
