//
// Created by jorn on 14-11-18.
//

#ifndef SHADERSENZO_SETTINGS_H
#define SHADERSENZO_SETTINGS_H

#include <string>

class Settings {

public:
    Settings();                                         // Load defaults
    Settings load();
    Settings loadDefault();  // Simply pass an instance of this object
    bool save();                    // Try to safe, return if successful

    int RESOLUTION_X = 1280;
    int RESOLUTION_Y = 720;
    double FRAME_TIME = 1.0/60;
    double MOUSE_SENSITIVITY = 0.0005;

    unsigned char KEY_FORWARD   = 'w';
    unsigned char KEY_BACKWARD  = 's';
    unsigned char KEY_LEFT      = 'a';
    unsigned char KEY_RIGHT     = 'd';
    unsigned char KEY_UP        = 'q';
    unsigned char KEY_DOWN      = 'z';

    // Rotations are counterclockwise...
    unsigned char KEY_ROLL_ADD      = 'Q';
    unsigned char KEY_ROLL_SUBTRACT = 'E';

private:

    Settings(std::string path);                         // Load settings from file
    static bool isValidSettingsFile(std::string path);  // Check if file meets specs of settings format etc..

};


#endif //SHADERSENZO_SETTINGS_H
