//
// Created by jorn on 14-11-18.
//

#ifndef SHADERSENZO_INPUT_H
#define SHADERSENZO_INPUT_H


#include "../Camera.h"
#include "Settings.h"
#include "../util/vec3.h"

class Input {

public:
    Input(Camera * cam, Settings * settings);
    vec3 moveDirection();
    void mouseMove(double dx, double dy);
    void mouseClick();
    void keyBoardDown(unsigned char key);
    void keyBoardUp(unsigned char key);
private:
    Camera * cameraTarget;
    Settings * settings;
    bool keyStates[255];
};


#endif //SHADERSENZO_INPUT_H
