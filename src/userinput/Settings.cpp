//
// Created by jorn on 14-11-18.
//

#include "Settings.h"

Settings Settings::load() {
    if(!isValidSettingsFile("./settings.json")) return loadDefault();
    else return Settings("./settings.json"); // TODO: Implement loading of file
}

Settings Settings::loadDefault() {
    return Settings();
}

bool Settings::isValidSettingsFile(std::string path) {
    return false; // TODO: Implement this....
}

bool Settings::save() {
    return false; // TODO: implement saving of settings to json
}

Settings::Settings() {
    // Dont override anything, defaults are already set
}

Settings::Settings(std::string path) {
    // TODO: read file, and replace settings in this object with file contents
}
