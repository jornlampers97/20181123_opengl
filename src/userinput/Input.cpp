//
// Created by jorn on 14-11-18.
//

#include "Input.h"

void Input::mouseMove(double dx, double dy) {
    cameraTarget->addYaw(dx);
    cameraTarget->addPitch(dy);
}

Input::Input(Camera * cam, Settings * settings) {
    cameraTarget = cam;
    this->settings = settings;
    for(int i = 0; i < sizeof(keyStates); i++)
        keyStates[i] = false;
}

void Input::keyBoardDown(unsigned char key) {
    keyStates[key] = true;

}

void Input::keyBoardUp(unsigned char key) {
    keyStates[key] = false;
}

vec3 Input::moveDirection() {
    cameraTarget->addRoll((keyStates[settings->KEY_ROLL_ADD] - keyStates[settings->KEY_ROLL_SUBTRACT])* 0.01);
    return
    vec3(
            keyStates[settings->KEY_RIGHT] - keyStates[settings->KEY_LEFT],
            keyStates[settings->KEY_UP] - keyStates[settings->KEY_DOWN],
            // As camera looks at (0, 0, -1) and z is the depth direction, Backwards in controls is forwards in worlds...
            keyStates[settings->KEY_BACKWARD] - keyStates[settings->KEY_FORWARD]
    ).normalize();
}
